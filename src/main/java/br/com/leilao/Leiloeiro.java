package br.com.leilao;

import java.util.List;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() throws RuntimeException{
        List<Lance> lances = leilao.getLances();
        if(lances.size() == 0){
            throw new RuntimeException("Ainda não tivemos lances");
        }
        Lance lance = lances.get(lances.size() - 1);
        return lance;
    }

    @Override
    public String toString() {
        return "Leiloeiro{" +
                "nome='" + nome + '\'' +
                leilao +
                '}';
    }
}
