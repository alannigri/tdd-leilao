package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances = new ArrayList<>();

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) {
        if(lances.size() != 0){
            validarLance(lance);
        }
        lances.add(lance);
        return lance;
    }

    public Lance validarLance(Lance novoLance) throws RuntimeException{
        Lance maiorLance = lances.get(lances.size()-1);
        if (novoLance.getValorDoLance() > maiorLance.getValorDoLance()) {
            return novoLance;
        } else {
            throw new RuntimeException("Lance é menor ou igual ao lance anterior");
        }
    }

    @Override
    public String toString() {
        return "Leilao{" +
                 lances +
                '}';
    }
}
