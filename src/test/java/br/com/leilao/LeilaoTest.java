package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

public class LeilaoTest {

    Leilao leilao;
    Usuario usuario;
    Lance lance;

    @BeforeEach
    public void criarLeilao() {
        leilao = new Leilao();
        usuario = new Usuario(1, "Alan");
        lance = new Lance(usuario, 20);
        leilao.adicionarNovoLance(lance);
    }

    @Test
    public void testarAdicionarLance() {
        List<Lance> listaDeLances = leilao.getLances();
        Assertions.assertEquals(lance, listaDeLances.get(listaDeLances.lastIndexOf(lance)));
    }

    @Test
    public void testarDarUmLanceMaiorDoQueUmJaExistente() {
        Lance lance1 = new Lance(usuario, 50);
        Lance novoLance = leilao.validarLance(lance1);
        Assertions.assertEquals(lance1, novoLance);
    }

    @Test
    public void testarDarUmLanceMenorDoQueUmJaExistente() {
        Lance lance1 = new Lance(usuario, 1);
        Assertions.assertThrows(RuntimeException.class, () -> {
            leilao.validarLance(lance1);
        });
    }


    @Test
    public void testarDarUmLanceDeValorIgualAoJaExistente() {
        Lance lance1 = new Lance(usuario, 20);
        Assertions.assertThrows(RuntimeException.class, () -> {
            leilao.validarLance(lance1);
        });
    }

}
