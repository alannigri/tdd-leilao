package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTest {

    Leilao leilao;
    Usuario usuario;
    Lance lance;
    Leiloeiro leiloeiro;

    @BeforeEach
    public void criarLeilao() {
        leilao = new Leilao();
        usuario = new Usuario(1, "Alan");
        leiloeiro = new Leiloeiro("Joao", leilao);
    }

    @Test
    public void testarORetornoDoMetodoDoLeiloeiro() {
        lance = new Lance(usuario, 20);
        leilao.adicionarNovoLance(lance);
        Usuario usuario2 = new Usuario(2, "Thais");
        Lance lance1 = new Lance(usuario2, 50);
        leilao.adicionarNovoLance(lance1);
        Lance maiorLance = leiloeiro.retornarMaiorLance();
        Assertions.assertEquals(maiorLance, lance1);
        System.out.println(maiorLance.toString());
    }

    @Test
    public void testarORetornoDoMetodoDoLeiloeiroSeAListaEstiverVazia() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            leiloeiro.retornarMaiorLance();
        });

    }
}
